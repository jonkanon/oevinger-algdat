function f(m,n)
    if m == 1 && n == 1
        return 0
    end
    if m == 1
        return 1
    elseif n == 1
        return 1
    else
        return f(m-1,n) + f(m,n-1)
    end
end


println(f(5,4))
