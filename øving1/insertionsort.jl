function insertionsort!(liste)
    for i in 2:length(liste)
        nøkkel = liste[i]
        j = i - 1
        while j > 0 && liste[j] > nøkkel
            liste[j+1] = liste[j]
            j = j-1
        end
        liste[j+1] = nøkkel
    end
end


liste = [5,2,4,6,1,3]
println(liste)
insertionsort!(liste);
println(liste)
