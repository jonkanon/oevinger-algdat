mutable struct Node
	children::Dict{Char,Node}
	count::Int
end

function brokendnasearch(node, dna, i=1)
    if i > length(dna)
        return node.count
    end
    if haskey(node.children, dna[i])
        return brokendnasearch(node.children[dna[i]], dna, i+1)
    elseif dna[i] == '?'
        sum = 0
        if haskey(node.children, 'A')
            sum += brokendnasearch(node.children['A'], dna, i+1)
        end
        if haskey(node.children, 'T')
            sum += brokendnasearch(node.children['T'], dna, i+1)
        end
        if haskey(node.children, 'G')
            sum += brokendnasearch(node.children['G'], dna, i+1)
        end
        if haskey(node.children, 'C')
            sum +=  brokendnasearch(node.children['C'], dna, i+1)
        end
        return sum
    else
        return 0
    end
end

# println("RESULTAT: ", brokendnasearch(tre, "???C"))
