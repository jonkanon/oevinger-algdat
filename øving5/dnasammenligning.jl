function dnasimilarity(s1, s2)
    like = 0
    for i in 1:length(s1)
        if s1[i] == s2[i] like += 1
        end
    end
    return like
end
