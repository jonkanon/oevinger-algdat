try

    mutable struct Node
        children::Dict{Char,Node}
        count::Int
    end
catch

end

Node() = Node(Dict(), 0)

### Denne funksjonen overlagrer/definerer likhet for Node-objektet
import Base: ==
(==)(a::Node, b::Node) = a.count == b.count && a.children == b.children

function buildtree(dnasequences)
    rot = Node()
    # Alle sekvenser har den tomme strengen som prefix:
    rot.count = length(dnasequences)

    for i in 1:length(dnasequences)
        nåværende = rot
        for j in 1:length(dnasequences[i])
            if !haskey(nåværende.children, dnasequences[i][j])
                nybib = Dict(dnasequences[i][j] => Node())
                merge!(nåværende.children, nybib)
            end
            nåværende = nåværende.children[dnasequences[i][j]]
            nåværende.count += 1
        end
    end
    return rot
end
