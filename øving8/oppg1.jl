include("node.jl")

labyrint = [1 1 0
            0 1 0
            1 1 1]

function mazetonodelist(labyrint)
    antallnoder = 0
    for rute in labyrint
        if rute == 1
            antallnoder += 1
        end
    end

    nodeliste = Array{Node, 1}(undef, antallnoder)
    t = 1

    # legg gulvnodene i nodelisten og eventuelle naboer
    for i in 1:size(labyrint, 1)
        for j in 1:size(labyrint, 2)
            if labyrint[i,j] == 1
                nodeliste[t] = Node(i,j)

                # sjekk noden over
                if i != 1
                    if labyrint[i-1, j] == 1
                        push!(nodeliste[t].neighbors, Node(i-1, j))
                    end
                end

                # sjekk noden under
                if i != size(labyrint,1)
                    if labyrint[i+1, j] == 1
                        push!(nodeliste[t].neighbors, Node(i+1,j))
                    end
                end

                # sjekk noden til venstre
                if j != 1
                    if labyrint[i, j-1] == 1
                        push!(nodeliste[t].neighbors, Node(i,j-1))
                    end
                end

                # sjekk noden til høyre
                if j != size(labyrint, 2)
                    if labyrint[i, j+1] == 1
                        push!(nodeliste[t].neighbors, Node(i,j+1))
                    end
                end
                t += 1
            end
        end
    end

    return nodeliste
end

# mazetonodelist(labyrint)
