using DataStructures: Queue, enqueue!, dequeue!
include("node.jl")

function bfs!(nodeliste, startnode)
    for node in nodeliste
        node.color = "white"
        node.distance = typemax(Int)
        node.predecessor = nothing
    end
    startnode.color = "gray"
    startnode.distance = 0

    kø = Queue{Node}()
    enqueue!(kø, startnode)

    while !isempty(kø)
        node = dequeue!(kø)
        if isgoalnode(node) return node end
        for nabo in node.neighbors
            if nabo.color == "white"
                nabo.color = "gray"
                nabo.distance = node.distance + 1
                nabo.predecessor = node
                enqueue!(kø, nabo)
            end
        end
        node.color = "black"
    end
    return nothing
end
