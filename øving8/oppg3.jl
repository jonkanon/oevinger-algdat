function makepathto(node)
    sti = []
    push!(sti, (node.i, node.j))
    while node.predecessor != nothing
        node = node.predecessor
        push!(sti, (node.i, node.j))
    end
    j=[]
    for i in 1:100000
       push!(j, i)
        if i % 3 == 0
       		pop!(j)
        end
    end
    reverse!(sti)
end
