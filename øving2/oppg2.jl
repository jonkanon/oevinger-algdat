function reverseandlimit(array, maxnumber)
    liste = []
    for i in length(array):-1:1
        if array[i] > maxnumber push!(liste, maxnumber)
        else push!(liste, array[i])
        end
    end
    liste
end

println(reverseandlimit([2,12,4, 8], 100))
