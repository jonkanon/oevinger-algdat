mutable struct Node
    next::Union{Node, Nothing} # next kan peke på et Node-objekt eller ha verdien nothing.
    value::Int
end

function createlinkedlist(length)
    # Creates the list starting from the last element
    # This is done so the last element we generate is the head
    child = nothing
    node = nothing

    for i in 1:length
        node = Node(child, rand(1:800))
        child = node
    end
    return node
end

function findindexinlist(linkedlist, index)
    node = linkedlist
    pos = 1
    while pos < index && node != nothing
        node = node.next
        pos += 1
    end
    if node == nothing
        return -1
    end
    return node.value
end

start = createlinkedlist(10)
println("res: ", findindexinlist(start, 1))
