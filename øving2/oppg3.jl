mutable struct NodeDoublyLinked
    prev::Union{NodeDoublyLinked, Nothing} # Er enten forrige node eller nothing
    next::Union{NodeDoublyLinked, Nothing} # Er enten neste node eller nothing
    value::Int # Verdien til noden
end

function createLinkedListDoublyLinked(length)
    # Create the list from the last element in the chain
    # This is so the returned element will be the first in the chain
    current = nothing
    beforeCurrent = nothing

    for i in 1:length
        # only fill out the next field because prev will be filled out later
        current = NodeDoublyLinked(nothing, beforeCurrent, rand(-100:100))
        # link up the node before this node to this node
        if beforeCurrent != nothing
            beforeCurrent.prev = current
        end
        beforeCurrent = current
    end
    tilfeldignode = rand(1:length)
    for i in 1:tilfeldignode
        if current.next != nothing
            current = current.next
        end
    end
    return current
end

function maxofdoublelinkedlist(linkedlist)
    stegmothøyre = 0
    stegmotvenstre = 0
    maks = linkedlist.value
    #gå frem i listen
    while linkedlist.next != nothing
        if linkedlist.value > maks maks = linkedlist.value end
        linkedlist = linkedlist.next
        stegmothøyre += 1
    end
    # gå bakover
    while linkedlist.prev != nothing
        if linkedlist.value > maks maks = linkedlist.value end
        linkedlist = linkedlist.prev
        stegmotvenstre += 1
    end
    # gå frem til start
    for i in 1:(stegmotvenstre-stegmothøyre+1)
        if linkedlist.value > maks maks = linkedlist.value end
        linkedlist = linkedlist.next
    end
    return maks
end

liste = createLinkedListDoublyLinked(1)
println(maxofdoublelinkedlist(liste))
