function mincoins(mynter, verdi)
    resultat = fill(typemax(Int), (1,verdi+1))
    brukbare = []
    for mynt in mynter
        if mynt <= verdi
            resultat[mynt] = 1
            push!(brukbare, mynt)
        end
    end
    for i in 1:verdi + 1
        if resultat[i] != typemax(Int)
            continue
        end
        best = typemax(Int)
        for mynt in brukbare
            if mynt <= i
                nåværende = 1 + resultat[i - mynt]
                if nåværende < best
                    best = nåværende
                end
            end
        end
        resultat[i] = best
    end
    return resultat[verdi]
end
println(mincoins([40, 30, 20, 10, 1], 90))
