function mincoinsgreedy(mynter, verdi)
    for i in 1:length(mynter)
        if mynter[i] <= verdi
            verdi -= mynter[i]
            return 1 + mincoinsgreedy(mynter, verdi)
            break
        end
    end
    return 0
end

# println(mincoinsgreedy([120, 60, 30, 6, 1],288))
# println(mincoinsgreedy([30, 6, 3, 1], 171))
