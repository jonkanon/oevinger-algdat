function countingsortlength(A)

    # finn lengste

    lengst = length(A[1])
    for i in 2:length(A)
        if length(A[i]) > lengst
             lengst = length(A[i])
        end
    end

    # lage C-listen med bare 0er
    C = []
    for i in 1:(lengst+1)
        push!(C, 0);
    end

    # teller
    for i in 1:length(A)
        C[length(A[i])+1] += 1
    end

    #akkumulerer
    for i in 2:length(C)
        C[i] += C[i-1]
    end

    # lage returtabellen med bare 0er
    returtabell = []
    for i in 1:C[length(C)]
        push!(returtabell, 0)
    end

    # plassere i returtabell
    for i in length(A):-1:1
        tall = length(A[i])
        pos = C[tall+1]
        returtabell[pos] = A[i]
        C[tall+1] -= 1
    end
    return returtabell
end

# println(length("hei"))
# println(countingsortlength(["a", "ablakjsydoiuw", "csads", "d"]))
