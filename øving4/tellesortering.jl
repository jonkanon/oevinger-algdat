function countingsortletters(A,position)
    størst = 0

    for i in 1:length(A)
        if chartodigit(A[i][position]) > størst
             størst = chartodigit(A[i][position])
        end
    end

    # lage C-listen med bare 0er
    C = []
    for i in 1:størst
        push!(C, 0);
    end

    # teller
    for i in 1:length(A)
        C[chartodigit(A[i][position])] += 1
    end

    #akkumulerer
    for i in 2:length(C)
        C[i] += C[i-1]
    end

    # lage returtabellen med bare 0er
    returtabell = []
    for i in 1:C[length(C)]
        push!(returtabell, "")
    end

    # plassere i returtabell
    for i in length(A):-1:1
        tall = chartodigit(A[i][position])
        pos = C[tall]
        returtabell[pos] = A[i]
        C[tall] -= 1
    end
    return returtabell
end


function chartodigit(character)
    #Dette er en hjelpefunksjon for å få omgjort en char til tall
    #Den konverterer 'a' til 1, 'b' til 2 osv.
    #Eksempel: chartodigit("hei"[2]) gir 5 siden 'e' er den femte bokstaven i alfabetet.

    return character - '`'
end
countingsortletters(["a", "b", "c", "d"], 1)
# countingsortletters(["ccc", "cba", "ca", "ab", "abc"], 2)

# println(chartodigit('z'))
