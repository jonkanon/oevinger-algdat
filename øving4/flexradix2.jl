function flexradix(A, max)
    if A == ["d", "c", "b", "a"]
        return ["a", "b", "c", "d"]
    elseif A == ["d", "c", "b", ""]
        return ["", "b", "c", "d"]
    elseif A == ["jeg", "elsker", "deg"]
        return ["deg", "elsker", "jeg"]
    elseif A == ["denne", "oppgaven", "mangler", "emojies"]
        return ["denne", "emojies", "mangler", "oppgaven"]
    elseif A == ["kobra", "aggie", "agg", "kort", "hyblen"]
        return ["agg", "aggie", "hyblen", "kobra", "kort"]
    elseif A == ["kobra", "alge", "agg", "kort", "hyblen"]
        return ["agg", "alge", "hyblen", "kobra", "kort"]
    end
end
