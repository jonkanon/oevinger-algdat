include("tellesortering.jl")
include("tellelengde.jl")

function flexradix(A, maxlength)
    #sorter på lengde
    A = countingsortlength(A);
    sisteindexmedlengde = length(A)
    for i in maxlength:-1:1
        if i == 0 break end
        for j in sisteindexmedlengde:-1:1
            if length(A[j]) >= i
                sisteindexmedlengde = j
            else break
            end
        end
        #sorter de siste
        sistesorterte = countingsortletters(A[sisteindexmedlengde:end], i)
        # legg de sorterte siste inn igjen
        A = vcat(A[1:sisteindexmedlengde-1], sistesorterte)
    end
    return A
end
println(flexradix(["kobra", "alge", "agg", "kort", "hyblen"], 6))
# println(flexradix(["d", "c", "b", ""], 1))
# println(flexradix(["d", "c", "b", "a"], 1))
# println(flexradix(["kobra", "aggie", "agg", "kort", "hyblen"], 6))
# rad = []
# println(vcat([1,2,3], [50,1,6]))

# println(length("heih"))
