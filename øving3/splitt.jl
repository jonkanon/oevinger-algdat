function splitintwo(x,y)
    xvenstre = reshape([],0,2)
    xhøyre = reshape([],0,2)
    yvenstre = reshape([],0,2)
    yhøyre = reshape([],0,2)
    midt = convert(Int64, ceil(size(x, 1)/2))
    xvenstre = x[1:midt, 1:2]
    xhøyre = x[midt+1:size(x,1), 1:2]

    for i in 1:size(y,1)
        for j in 1:size(xvenstre,1)
            if y[i, 1:2] == xvenstre[j, 1:2]
                yvenstre = vcat(yvenstre, [y[i, 1] y[i, 2]])
                break
            elseif j == size(xvenstre,1)
                yhøyre = vcat(yhøyre, [y[i, 1] y[i, 2]])
            end
        end
    end
return xvenstre, xhøyre, yvenstre, yhøyre

end

x = [1.0 2.0; 2.0 3.0; 3.0 2.0; 4.0 5.0; 6.0 6.0; 7.0 1.0]
y = [7.0 1.0; 1.0 2.0; 3.0 2.0; 2.0 3.0; 4.0 5.0; 6.0 6.0]

println(splitintwo(x, y))
