#x og y er to sorterte arrays, koordinat angir koordinat akse
function mergearrays(x,y,koordinat)
    retur = reshape([],0,2)
    #legg til uendelig
    xPeker = 1
    yPeker = 1
    x = vcat(x, [(2^63 - 1) (2^63 - 1)])
    y = vcat(y, [(2^63 - 1) (2^63 - 1)])
    while xPeker != size(x, 1) || yPeker != size(y, 1)
        if x[xPeker, koordinat] <= y[yPeker, koordinat]
            retur = vcat(retur, [(x[xPeker,1]) (x[xPeker, 2])])
            xPeker += 1
        else
            retur = vcat(retur, [(y[yPeker,1]) (y[yPeker, 2])])
            yPeker += 1
        end
    end
    return retur
end

#x usortert array, koordinat angir koordinat akse vi skal sortere langs
function mergesort(x, koordinat)
    if size(x, 1) > 1
        midt = convert(Int64, floor(size(x, 1)/2))
        a = x[1:midt, 1:2]
        b = x[midt+1:size(x, 1), 1:2]
        a = mergesort(a, koordinat)
        b = mergesort(b, koordinat)
        x = mergearrays(a, b, koordinat)
    end
    return x
end

a = [1 2; 4 5; 3 2; 2 2; 7 1; 5 5]
b = [1 2]
c = [2 1; 3 3]

println(mergesort(a, 1))
