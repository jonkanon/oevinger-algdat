function binaryintervalsearch(x, delta, koordinat)
    # println(size(x,1)%2)
    if size(x, 1) % 2 == 1
        median = x[convert(Int64, ceil(size(x, 1)/2)), koordinat]
    else
        median = (x[convert(Int64, size(x, 1)/2), koordinat] + x[convert(Int64, size(x, 1)/2 + 1), koordinat])/2
    end
println("median: ", median)
    nedre = median - delta
    øvre = median + delta
    minst = -1.0
    størst = -1.0

    println("ser etter nedre")
    for i in convert(Int64, ceil(size(x, 1)/2)):-1:1
        println("i:", i)
        println("nedre: ", nedre)
        println("verdi:", x[i, koordinat])
        println()
        if x[i, koordinat] <= øvre && x[i, koordinat] >= nedre
            # minst = x[i, koordinat]
            minst = i
        else
            break
        end
    end

    println("ser etter øvre")
    for i in convert(Int64, ceil(size(x, 1)/2)):size(x, 1)
        println("i:", i)
        println("øvre: ", øvre)
        println("verdi:", x[i, koordinat])
        println()
        if x[i, koordinat] <= øvre && x[i, koordinat] >= nedre
            println()
            println("FANT øvre", x[i, koordinat], " er <= ", øvre)
            println()
            # størst = x[i, koordinat]
            størst = i
        else
            break
        end
    end

    # println(median)
    if størst != -1 println("størst: ", størst, ", verdi: ", x[størst, koordinat]) end
    if minst != -1 println("minst: ", minst, ", verdi:", x[minst, koordinat]) end
    return minst, størst
end

a = [1 0; 2 0; 2 0; 3 0; 4 0; 5 0; 5 0]
b = [1 2; 2 0; 3 3; 4 4]
c = [1 2; 2 3; 3 0; 4 0; 5 1]
d = [1.0 0.0; 2.0 0.0; 3.0 0.0]
e = [1.0 0.0; 2.0 0.0; 3.0 0.0; 4.0 0.0]
println(binaryintervalsearch(e, 0.1, 1))
