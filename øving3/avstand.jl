function avstandf(a, b)

    katet1 = a[1] - b[1]
    katet2 = a[2] - b[2]
    avstand = sqrt(katet1^2 + katet2^2)
    println(a, b)
    println(avstand)
    return avstand
end

function bruteforce(x)
    kortest = avstandf(x[1,:], x[2,:])
    avstand = 0
    for i in 1:size(x,1)
        for j in i+1:size(x,1)
            avstand = avstandf(x[i,:], x[j,:])
            if avstand < kortest
                kortest = avstand
            end
        end
        println()
    end
    return kortest
end

x =  [1 1; 10 0; 2 2; 5 5]
println(bruteforce(x))
